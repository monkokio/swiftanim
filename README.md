﻿Swift Anim for Unity
created by Bryan Singh
send feedback to monkokio@gmail.com

=========

Swift Anim is a Flash sprite importer for Unity. It takes .swf animations made in Adobe Flash and converts them to Unity Game Objects.

Think of Swift Anim as more of an animation importer, and less of a Flash player. It intelligently renders your Flash animations to textures and creates Unity Game Objects that match your your Flash Movie Clips. As much as possible, Swift Anim uses native Unity systems for Game Objects, Sprites, and Animations.

Swift aim is not a Flash player. It does not support Action Script, and it does not render vector graphics. It does not have custom “MovieClip” or “Timeline” components. It does not try to make your Unity scripts look like Flash scripts.

=========

Importing Flash using Swift Anim:
Simply drop a .swf file anywhere in your Assets folder and Swift Anim will generate a folder with Game Object prefabs ready for you to use.
Any animations and Movie Clips found on your Flash stage will be imported. Any Movie Clip “instance names” and “class names” will be preserved.

=========

How Does it work?
When Swift Anim finds a .swf file, it automatically triggers a two-step import process.
Step 1. A custom program will read your SWF file, render textures, and generate a temporary XML file that describes your animation. This program uses Adobe AIR so it can rely an Adobe’s official technology for reading and rendering SWF files.
Step 2. The temporary XML file is brought into Unity for sprites and Game Object prefabs to be created

=========

Installation:
Include the “SwiftAnim” directory in your Unity project Assets folder.
Whenever you add a .swf file to your Unity project Assets folder, Swift Anim will process and generate prefabs from the .swf file.

=========

Features:
 - Converts Flash Movie Clips to Unity Game Objects
 - Flash Movie Clip class names (specified in your Flash library), are used as Unity Prefab names
 - Flash Movie Clip instance names are used as Unity Game Object instance names
 - Flash timeline labels are used to generate Unity Animator state machines

=========

Unsupported Features:
 - No filters support (tints, blurs drop shadows)
 - No ActionScript support
 - No dynamic text support

=========

Known Issues:
- Grouped Sorting. Overlapping game objects can be sorted weird. Use the "Order Child Sprites" component to set the sorting order. When Unity’s "Sorting Group" component is released, this will go away.
- Nested Prefabs. In Flash, Movie Clips can be referenced in multiple places. Because Unity does not support nested prefabs, Game Objects that are referenced in multiple places are clones, not references.

=========

Parts of the 7-Zip program are used. 7-Zip is licensed under the GNU LGPL license. 7-Zip source code can be found at www.7-zip.org

=========

Full Source code online at:
https://bitbucket.org/monkokio/swiftanim

License: Totally free. do what you want with it.

The MIT License (MIT)
Copyright (c) 2016 Bryan Singh
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.