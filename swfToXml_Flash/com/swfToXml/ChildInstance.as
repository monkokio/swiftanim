﻿package com.swfToXml  {
	
	public class ChildInstance {

		public var name : String = "";
		public var prefabName : String = "";
		public var posX : Number = 0;
		public var posY : Number = 0;
		public var scaleX : Number = 0;
		public var scaleY : Number = 0;
		public var rotation : Number = 0;
		public var childIndex : Number = 0;
		public var type : String = "undefined"; // "movieClip" or "shape"
		
		public function ChildInstance() {
			// constructor
		}

	}
	
}