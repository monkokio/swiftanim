﻿package com.swfToXml {
	
	import com.swfToXml.*;
	
	public class MovieClipPrefab {

		public var name:String = null;
		public var frames:Vector.<MovieClipPrefabFrame> = new Vector.<MovieClipPrefabFrame>();
		
		public function MovieClipPrefab() {
			
		}

	}
	
}
