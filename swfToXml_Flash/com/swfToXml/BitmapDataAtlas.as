﻿package com.swfToXml
{
	import flash.display.BitmapData;
	import flash.geom.Point;

	public class BitmapDataAtlas
	{

		public static var MIN_TEXTURE_SIZE : Number = 128;
		public static var MAX_TEXTURE_SIZE : Number = 2048;
		
		public var atlasAllocationSize: Number = 2;
		private var atlasAllocationStepSize: Number = 1;

		public var allocationBuffer: Vector.< Vector.< Boolean >>;
		public var bitmapData: BitmapData = null;
		public var fileName : String = "";

		public function BitmapDataAtlas( _fileName : String = "texture.png" )
		{
			bitmapData = new BitmapData(BitmapDataAtlas.MIN_TEXTURE_SIZE, BitmapDataAtlas.MIN_TEXTURE_SIZE, true, 0x00000000);
			allocationBuffer = NewAllocationBuffer(bitmapData.width);
			fileName = _fileName;
		}

		private function NewAllocationBuffer(bitmapSize: Number): Vector.< Vector.< Boolean >>
		{
			var newBuffer: Vector.< Vector.< Boolean >> = new Vector.< Vector.< Boolean >> ();

			for(var x = 0; x < bitmapSize / atlasAllocationSize; x++)
			{
				newBuffer.push(new Vector.< Boolean > ());
				for(var y = 0; y < bitmapSize / atlasAllocationSize; y++)
				{
					newBuffer[x].push(false);
				}
			}
			return newBuffer;
		}
		private function GrowSize()
		{
			if( this.bitmapData.height * 2 > BitmapDataAtlas.MAX_TEXTURE_SIZE ){
				return false;
			}
			
			// copy bitmap into new bitmap
			var newBitmapData: BitmapData = new BitmapData(this.bitmapData.width * 2, this.bitmapData.height * 2, true, 0x00000000);
			newBitmapData.copyPixels(this.bitmapData, this.bitmapData.rect, new Point());
			this.bitmapData = newBitmapData;

			// copy allocation data to new buffer
			var newAllocationBuffer: Vector.< Vector.< Boolean >> = NewAllocationBuffer(newBitmapData.width);
			for(var x = 0; x < this.allocationBuffer.length; x++)
			{
				for(var y = 0; y < this.allocationBuffer[x].length; y++)
				{
					newAllocationBuffer[x][y] = this.allocationBuffer[x][y];
				}
			}
			this.allocationBuffer = newAllocationBuffer;
			if( this.bitmapData.width >= 255 ){
				this.atlasAllocationStepSize ++;
			}
			return true;

		}
		public function AddBitmapData(newBitmapData: BitmapData): Point
		{
			var position: Point = FindAvailableSpace(newBitmapData.width, newBitmapData.height);
			if( position != null ){
				this.allocateBlock(position.x, position.y, newBitmapData.width, newBitmapData.height);
				this.bitmapData.copyPixels(newBitmapData, newBitmapData.rect, position, null, null, true);
			}
			return position;
		}
		private function allocateBlock(pixelsX: Number, pixelsY: Number, pixelsWidth: Number, pixelsHeight: Number)
		{
			var indexX: Number = Math.floor(pixelsX / this.atlasAllocationSize);
			var indexY: Number = Math.floor(pixelsY / this.atlasAllocationSize);
			var indexWidth: Number = Math.floor(pixelsWidth / this.atlasAllocationSize) + 1;
			var indexHeight: Number = Math.floor(pixelsHeight / this.atlasAllocationSize) + 1;
			for(var x = indexX; x < indexX + indexWidth; x++)
			{
				for(var y = indexY; y < indexY + indexHeight; y++)
				{
					this.allocationBuffer[x][y] = true;
				}
			}
		}
		private function FindAvailableSpace(pixelsWidth: Number, pixelsHeight: Number): Point
		{
			for(var x = 0; x < this.allocationBuffer.length; x += this.atlasAllocationStepSize)
			{
				for(var y = 0; y < this.allocationBuffer.length; y += this.atlasAllocationStepSize)
				{
					if(IsSpaceAvailable(x, y, pixelsWidth, pixelsHeight))
					{
						return new Point(x * this.atlasAllocationSize, y * this.atlasAllocationSize);
					}
				}
			}
			if( this.GrowSize() ){
				return FindAvailableSpace(pixelsWidth, pixelsHeight);
			} else {
				return null;
			}
			
		}
		private function IsSpaceAvailable(indexX: Number, indexY: Number, pixelsWidth: Number, pixelsHeight: Number): Boolean
		{
			var indexWidth = Math.floor(pixelsWidth / this.atlasAllocationSize) + 1;
			var indexHeight = Math.floor(pixelsHeight / this.atlasAllocationSize) + 1;

			if(indexX + indexWidth >= this.allocationBuffer.length || indexY + indexHeight >= this.allocationBuffer.length)
			{
				return false;
			}

			for(var x = indexX; x < indexX + indexWidth; x+= this.atlasAllocationStepSize)
			{
				for(var y = indexY; y < indexY + indexHeight; y+= this.atlasAllocationStepSize)
				{
					if(this.allocationBuffer[x][y])
					{
						return false;
					}
				}
			}
			return true;
		}
	}

}