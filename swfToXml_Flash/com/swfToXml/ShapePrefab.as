﻿package com.swfToXml
{

	import flash.display.*;

	public class ShapePrefab
	{
		// members
		public var bitmapData: BitmapData = null;
		public var width: Number = 0;
		public var height: Number = 0;
		public var originX: Number = 0;
		public var originY: Number = 0;
		public var name: String = null;
		public var textureName: String = "";		
		public var textureX: Number = 0;
		public var textureY: Number = 0;

		public function ShapePrefab()
		{
		}
	}
}