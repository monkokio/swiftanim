﻿package com.swfToXml
{

	import fl.controls.*;
	import flash.net.*;
	import flash.events.*;
	import flash.display.*;
	import flash.utils.*;
	import flash.geom.*;
	import flash.filesystem.*;
	import flash.system.*;
	import flash.desktop.*;
	import com.swfToXml.*;
	import com.adobe.images.PNGEncoder;
	
	public class Main extends MovieClip
	{

		public static var instance: Main = null;

		// constants
		const BITMAP_PADDING: Number = 1;
		const VERSION : String = "1.0";

		// automatically assigned from stage:
		public var textOutput: TextArea;
		public var filePathText: TextInput;
		public var fileBrowseButton: Button;
		public var crunchButton: Button;
		public var progressBar: ProgressBar;
		public var statusText : Label;
		

		// private vars
		private var file: File = null;
		private var swfLoader: Loader = null;
		private var fileSelected: Boolean = false;

		private var MovieClipPrefabs: Vector.< MovieClipPrefab > = new Vector.< MovieClipPrefab > ();
		private var ShapePrefabs: Vector.< ShapePrefab > = new Vector.< ShapePrefab > ();

		public function Main()
		{
			Main.instance = this;

			this.file = new File();
			this.swfLoader = new Loader();

			this.fileBrowseButton.addEventListener(MouseEvent.CLICK, BrowseForFile);
			this.crunchButton.addEventListener(MouseEvent.CLICK, ExportButtonPressed);
			
			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvoke);  
		}
		
		private var calledFromCommandLine : Boolean = false;
		private var commandLineArgs : Vector.<String> = new Vector.<String>();
		public function onInvoke(e:InvokeEvent):void {
			
			this.progressBar.mode = ProgressBarMode.MANUAL;
			this.progressBar.setProgress( 0, 100 );

			Main.DebugPrint("Initialize v" + VERSION );
			
			if( e.arguments.length > 0 ){
				DebugPrint( "Processing " + e.arguments.length + " files specified from command line" );
				this.calledFromCommandLine = true;
				for each( var arg : String in e.arguments ){
					commandLineArgs.push( arg );
				}
				this.ProcessNextCommandLineArg();
			}
		}
		public function ProcessNextCommandLineArg():void{
			if( this.commandLineArgs.length > 0 ){
				var arg : String = this.commandLineArgs.pop();
				this.file = new File(arg);
				this.fileSelected = true;
				this.filePathText.text = this.file.name;
				this.file.addEventListener(Event.COMPLETE, FileLoadComplete);
				this.file.load();
			}else{
				NativeApplication.nativeApplication.exit();
			}
		}
		
		public static function DebugPrint(str: String)
		{
			instance.statusText.text = str;
		}

		private function BrowseForFile(event: MouseEvent)
		{
			
			this.file.browse([new FileFilter("Flash SWF File", "*.swf")]);
			this.file.addEventListener(Event.SELECT, FileSelected);
		}
		
		private function FileSelected(event: Event)
		{
			this.fileSelected = true;
			this.filePathText.text = this.file.name;
			Main.DebugPrint("Loaded file: " + file.name);

		}


		private function ExportButtonPressed(event: MouseEvent)
		{
			if(this.fileSelected)
			{
				this.file.load();
				this.file.addEventListener(Event.COMPLETE, FileLoadComplete);
			}
			else
			{
				Main.DebugPrint("File must be selected first");
			}
		}
		private function FileLoadComplete(event: Event)
		{
			this.crunchButton.enabled = false;
			this.fileBrowseButton.enabled = false;
			this.filePathText.enabled = false;
			this.progressBar.setProgress( 5, 100 );
			Main.DebugPrint("Loading Swf File");
			var loaderContext: LoaderContext = new LoaderContext();
			loaderContext.allowLoadBytesCodeExecution = true; 
			this.swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, SwfLoadComplete);
			swfLoader.loadBytes(this.file.data, loaderContext);
		}
		private function SwfLoadComplete(event: Event)
		{
			this.progressBar.setProgress( 10, 100 );
			Main.DebugPrint("Parsing SWF and generating MC Prefabs");
			this.AfterOneFrame( this.ExportLoadedSwf );
		}
		private function ExportLoadedSwf()
		{
			// clear out prefabs
			this.MovieClipPrefabs = new Vector.< MovieClipPrefab > ();
			this.ShapePrefabs = new Vector.< ShapePrefab > ();
			
			// generate prefabs
			for(var i = 0; i < swfLoader.numChildren; i++)
			{
				var child: DisplayObject = swfLoader.getChildAt(i);
				if(child is MovieClip)
				{
					this.GetMovieClipPrefab(child as MovieClip, file.name.replace(".swf","_swf"));
				}
			}
			
			// generate texture atlas
			this.progressBar.setProgress( 30, 100 );
			Main.DebugPrint("Rendering and packing textures");
			this.AfterOneFrame( this.GenerateSpriteTextureAtlas );
			
		}
		
		private var textureAtlasses : Vector.<BitmapDataAtlas> = null;
		private var textureBaseName : String = null;
		private var prefabsToRender : Vector.< ShapePrefab > = null;
		private var totalprefabsToRender : Number = 0;
		
		private function GenerateSpriteTextureAtlas(): void
		{
			
			// start an array of texture atlases
			this.textureAtlasses = new Vector.<BitmapDataAtlas>();
			this.textureBaseName = file.name.replace(".swf","") + File.separator + "_textures" + File.separator + file.name.replace(".swf","");
			this.prefabsToRender = this.ShapePrefabs.concat();
			this.totalprefabsToRender = this.prefabsToRender.length;
			this.textureAtlasses.push( new BitmapDataAtlas( this.textureBaseName + this.textureAtlasses.length + ".png") );
			this.GenerateSpriteTextureAtlas_step();
		}
		private function GenerateSpriteTextureAtlas_step(): void
		{
			// iterate through all shapes
			
			if(this.prefabsToRender.length > 0)
			{
				// get the largest shape that needs to be put into an atlas
				var shapePrefab: ShapePrefab = this.PopBiggestShapePrefab(this.prefabsToRender);
				var texturePosition: Point = null;
				var fileName: String = "";
				
				// go through each texture atlas, and find one that can fit this shape
				for each( var atlas: BitmapDataAtlas in this.textureAtlasses ){
					texturePosition = atlas.AddBitmapData(shapePrefab.bitmapData);
					if( texturePosition != null ){
						fileName = atlas.fileName;
						break;
					}
				}
				
				// if no texture atlas can fit this shape, make a new one
				if( texturePosition == null ){
					var newAtlas :BitmapDataAtlas = new BitmapDataAtlas( this.textureBaseName + this.textureAtlasses.length + ".png");
					this.textureAtlasses.push( newAtlas );
					texturePosition = newAtlas.AddBitmapData(shapePrefab.bitmapData);
					fileName = newAtlas.fileName;
				}
				
				// if we STILL have no texture atlases that can fit this shape, cry
				if( texturePosition == null ){
					Main.DebugPrint( "The shape: " + shapePrefab.name + "is bigger than the max texture size. I have no idea what to do :(" );
				}else{
					shapePrefab.textureX = texturePosition.x;
					shapePrefab.textureY = texturePosition.y;
					shapePrefab.textureName = fileName;
				}
				
				var percentRendered : Number = (this.totalprefabsToRender - this.prefabsToRender.length) / this.totalprefabsToRender;
				this.progressBar.setProgress( 30 + 50.0 * percentRendered , 100 );
				this.AfterOneFrame( this.GenerateSpriteTextureAtlas_step );
			}
			else{
				
				this.progressBar.setProgress( 80, 100 );
				Main.DebugPrint("Saving Textures");
				this.AfterOneFrame( this.GenerateSpriteTextureAtlas_save );
			}
		}
		private function GenerateSpriteTextureAtlas_save(): void
		{
			// save all PNG files
			for each( var atlas: BitmapDataAtlas in this.textureAtlasses ){
				var fs : FileStream = new FileStream();
				var pngFile : File = file.parent.resolvePath( atlas.fileName );
				fs.open(pngFile, FileMode.WRITE);
				fs.writeBytes(PNGEncoder.encode(atlas.bitmapData));
				fs.close();
			}
			
			this.progressBar.setProgress( 90, 100 );
			Main.DebugPrint("Generating XML file");
			this.AfterOneFrame( this.GenerateXML );
			
		}
		
		private function GenerateXML(){
			
			// generate xml file
			var myXML:XML = new XML( <swfData /> );
			myXML.@fps = MovieClip( swfLoader.getChildAt(0) ).loaderInfo.frameRate;
			var i:int = 0;
			
			// go through shape prefabs
			var shapePrefabsXML:XML = new XML( <shapePrefabs /> );
			myXML.appendChild( shapePrefabsXML );			
			
			for( i = 0; i < this.ShapePrefabs.length; ++i ){				
				var shapeXml:XML = new XML(<shape />);
				shapePrefabsXML.appendChild( shapeXml );
				
				var shapePrefab:ShapePrefab = this.ShapePrefabs[i];
				shapeXml.@name = shapePrefab.name;
				shapeXml.@textureName = shapePrefab.textureName;					
				shapeXml.@textureX = shapePrefab.textureX;
				shapeXml.@textureY = shapePrefab.textureY;
				shapeXml.@textureWidth = shapePrefab.width;
				shapeXml.@textureHeight = shapePrefab.height;
				shapeXml.@originX = shapePrefab.originX;
				shapeXml.@originY = shapePrefab.originY;	
				
			}
			
			// go through MovieClip prefabs
			var mcPrefabsXML:XML = new XML( <movieClipPrefabs /> );
			myXML.appendChild( mcPrefabsXML );			
			
			for( i = 0; i < this.MovieClipPrefabs.length; ++i ){				
				var mcXML:XML = new XML(<movieClip />);
				mcPrefabsXML.appendChild( mcXML );
				
				var mcPrefab:MovieClipPrefab = this.MovieClipPrefabs[i];
				mcXML.@name = mcPrefab.name;
				
				var framesXML:XML = new XML(<frames />);
				mcXML.appendChild( framesXML );
				
				for( var f:int = 0; f < mcPrefab.frames.length; ++f ){
					var frameXML:XML = new XML(<frame />);
					framesXML.appendChild( frameXML );
					
					var frame:MovieClipPrefabFrame = mcPrefab.frames[f];
					frameXML.@frameNumber = f;
					frameXML.@label = frame.label;
					
					var childInstances:XML = new XML( <childInstances /> );
					frameXML.appendChild( childInstances );
					for each( var childInstance:ChildInstance in frame.childInstances ){
						var childInstanceXML:XML = new XML(<child />);
						childInstances.appendChild( childInstanceXML );
						
						childInstanceXML.@zIndex = childInstance.childIndex;
						childInstanceXML.@type = childInstance.type;
						childInstanceXML.@name = childInstance.name;
						childInstanceXML.@prefab = childInstance.prefabName;
						childInstanceXML.@x = childInstance.posX;
						childInstanceXML.@y = childInstance.posY;
						childInstanceXML.@scaleX = childInstance.scaleX;
						childInstanceXML.@scaleY = childInstance.scaleY;
						childInstanceXML.@rotation = childInstance.rotation;
					}
				}
				
			}
				
			//var xmlFile:FileReference = new FileReference();
			//xmlFile.save( myXML, file.name.replace(".swf", ".xml"));
			
			// save png file
			
			var fs : FileStream = new FileStream();
			var xmlFile : File = file.parent.resolvePath( file.name.replace(".swf",".xml") );
			fs.open(xmlFile, FileMode.WRITE);
			fs.writeUTFBytes( myXML.toXMLString() );
			fs.close();
			
			this.progressBar.setProgress( 0, 100 );
			Main.DebugPrint("Done!");
			this.crunchButton.enabled = true;
			this.fileBrowseButton.enabled = true;
			this.filePathText.enabled = true;
			
			if( this.calledFromCommandLine ){
				ProcessNextCommandLineArg();
			}
		}

		private function GetMovieClipPrefab(obj: MovieClip, desiredPrefabName:String = null): MovieClipPrefab
		{

			var prefab: MovieClipPrefab = this.FindMovieClipPrefab(obj);
			if(prefab == null)
			{
				prefab = GenerateMovieClipPrefab(obj, desiredPrefabName);
			}
			return prefab;
		}
		private function GenerateMovieClipPrefab(obj: MovieClip, desiredPrefabName:String = null): MovieClipPrefab
		{

			// Create a prefab from this MC
			var prefab: MovieClipPrefab = new MovieClipPrefab();
			prefab.name = this.GetPrefabNameFromMc(obj, desiredPrefabName);
			
			obj.gotoAndStop(obj.totalFrames);

			// go through each frame of the MC
			for(var frame = 0; frame < obj.totalFrames; frame++)
			{
				var mcFrame: MovieClipPrefabFrame = new MovieClipPrefabFrame();
				obj.gotoAndStop(frame + 1);
				if( obj.currentLabel ){
					mcFrame.label = obj.currentLabel;
				} else{
					mcFrame.label = "idle";
				}

				
				for(var i = 0; i < obj.numChildren; i++)
				{
					var child: DisplayObject = obj.getChildAt(i);
					var childInstance: ChildInstance = new ChildInstance();
					
					childInstance.posX = child.x;
					childInstance.posY = child.y;
					childInstance.scaleX = child.scaleX;
					childInstance.scaleY = child.scaleY;
					childInstance.rotation = child.rotation;
					childInstance.childIndex = i;
					
					// this is a weird hack. Shape rotation/scale gives unexpected values for nested graphics, so we instead extract from transformation matrix
					var xAxis:Point = new Point( child.transform.matrix.a, child.transform.matrix.b ); 
					var yAxis:Point = new Point( child.transform.matrix.c, child.transform.matrix.d );
					var mirrored:Boolean = xAxis.x * yAxis.y - xAxis.y * yAxis.x < 0.0; // do cross product to check if no longer right-handed
					var matScaleX:Number = Math.sqrt( Math.pow( xAxis.x, 2.0 ) + Math.pow( xAxis.y, 2.0 ) );
					var matScaleY:Number = Math.sqrt( Math.pow( yAxis.x, 2.0 ) + Math.pow( yAxis.y, 2.0 ) );
					var matRot:Number = (mirrored? Math.atan2( -xAxis.y, -xAxis.x ) : Math.atan2( xAxis.y, xAxis.x )) * 180.0 / Math.PI;
					var matX:Number = child.transform.matrix.tx;
					var matY:Number = child.transform.matrix.ty;
					childInstance.rotation = matRot;
					childInstance.scaleX = (mirrored ? -matScaleX : matScaleX);
					childInstance.scaleY = matScaleY;
					childInstance.posX = matX;
					childInstance.posY = matY;
					
					
					//Main.DebugPrint( child.name + ": " + flash.utils.getQualifiedClassName( child ) + " (rot: " + child.rotation + ", "  + matRot + ")" );
					
					if(child is MovieClip) {
						// find the prefab for this child MC
						var childMcPrefab: MovieClipPrefab = GetMovieClipPrefab(child as MovieClip);
						childInstance.prefabName = childMcPrefab.name;

						// gather child instance info
						childInstance.name = child.name.replace( "instance", childMcPrefab.name + "_" );
						childInstance.type = "movieClip";

						// save this child MC to the current frame
						mcFrame.childInstances.push(childInstance);
					}
					else if(child is Shape) {
						//find the prefab for this child Shape
						var childShapePrefab: ShapePrefab = GetShapePrefab(child as Shape, prefab.name );
						childInstance.prefabName = childShapePrefab.name;

						// gather child instance info
						childInstance.name = childShapePrefab.name + "_" + i;
						childInstance.type = "shape";
					}
					else {
						Main.DebugPrint("ERROR: " + getQualifiedClassName(obj) + " NOT SUPPORTED");
					}
					
					mcFrame.childInstances.push(childInstance);
				}

				prefab.frames.push(mcFrame);
			}
			this.MovieClipPrefabs.push(prefab);
			return prefab;
		}
		private function GetPrefabNameFromMc(movieClip: MovieClip, desiredPrefabName:String = null): String
		{
			var prefabName: String = "";
			if(getQualifiedClassName(movieClip) != "flash.display::MovieClip")
			{
				prefabName = getQualifiedClassName(movieClip);
			}
			else if( desiredPrefabName != null ){
				prefabName = desiredPrefabName;
			}
			else
			{
				prefabName = movieClip.name;
			}
			return prefabName;
		}
		private function FindMovieClipPrefab(movieClip: MovieClip)
		{
			var desiredPrefabName: String = this.GetPrefabNameFromMc(movieClip);

			for(var i = 0; i < this.MovieClipPrefabs.length; i++)
			{
				var prefab: MovieClipPrefab = this.MovieClipPrefabs[i];
				if(prefab.name == desiredPrefabName)
				{
					return prefab;
				}
			}
			return null;
		}
		private function GetShapePrefab(obj: Shape, parentName:String): ShapePrefab
		{
			var prefab: ShapePrefab = new ShapePrefab();

			var bounds: Rectangle = obj.getBounds(obj);
			prefab.width = Math.ceil( bounds.width + BITMAP_PADDING * 2);
			prefab.height = Math.ceil( bounds.height + BITMAP_PADDING * 2)
			prefab.originX = -bounds.x + BITMAP_PADDING;
			prefab.originY = -bounds.y + BITMAP_PADDING;
			
			//generate unique prefab name
			var uniqueNameIndex:int = 0;
			var uniqueName:String = "";
			do{
				var prefabNameAlreadyExists : Boolean = false;
				uniqueName = parentName + uniqueNameIndex;
				uniqueNameIndex++;
				for each( var existingShapePrefab:ShapePrefab in ShapePrefabs ){
					if( existingShapePrefab.name == uniqueName ){
						prefabNameAlreadyExists = true;
					}
				}
			}while(prefabNameAlreadyExists);
			
			prefab.name =  uniqueName;

			prefab.bitmapData = new BitmapData(prefab.width, prefab.height, true, 0x00000000);
			var bitmapTransform: Matrix = new Matrix();
			bitmapTransform.translate(BITMAP_PADDING - bounds.x, BITMAP_PADDING - bounds.y);
			prefab.bitmapData.draw(obj, bitmapTransform);

			var existingPrefab: ShapePrefab = FindMatchingShapePrefab(prefab);
			if(existingPrefab == null)
			{
				this.ShapePrefabs.push(prefab);
				return prefab;
			}
			else
			{
				return existingPrefab;
			}
		}
		private function FindMatchingShapePrefab(newPrefab: ShapePrefab): ShapePrefab
		{
			for(var i = 0; i < this.ShapePrefabs.length; i++)
			{
				var prefab: ShapePrefab = this.ShapePrefabs[i];
				if(this.DoShapePrefabsMatch(newPrefab, prefab))
				{
					return prefab;
				}
			}
			return null;
		}
		private function DoShapePrefabsMatch(prefab1: ShapePrefab, prefab2: ShapePrefab): Boolean
		{
			if(Math.abs(prefab1.width - prefab2.width) > 2.0)
			{
				return false;
			}
			if(Math.abs(prefab1.height - prefab2.height) > 2.0)
			{
				return false;
			}
			if(Math.abs(prefab1.originX - prefab2.originX) > 2.0)
			{
				return false;
			}
			if(Math.abs(prefab1.originY - prefab2.originY) > 2.0)
			{
				return false;
			}
			if(this.BitmapMatchScore(prefab1.bitmapData, prefab2.bitmapData) < 0.95)
			{
				return false;
			}


			return true;
		}
		private function BitmapMatchScore(bitmap1: BitmapData, bitmap2: BitmapData): Number
		{

			var diff: Object = bitmap1.compare(bitmap2);
			if(diff is Number)
			{
				var diffNumber: Number = diff as Number;
				if(diffNumber == 0)
				{
					return 1.0;
				}
				else if(diffNumber <= -1)
				{
					return 0.0;
				}
			}
			else if(diff is BitmapData)
			{

				var diffBitmap: BitmapData = diff as BitmapData;
				var matchScore: Number = 0;
				var matchScoreMax: Number = bitmap1.width * bitmap1.height;

				for(var x = 0; x < bitmap1.width; x++)
				{
					for(var y = 0; y < bitmap1.height; y++)
					{
						if(diffBitmap.getPixel(x, y) == 0)
						{
							matchScore++;
						}
					}
				}
				return matchScore / matchScoreMax;
			}
			return 0.0;
		}
		private function PopBiggestShapePrefab(prefabs: Vector.< ShapePrefab > ): ShapePrefab
		{
			var biggestSize: Number = 0;
			var biggestPrefab: ShapePrefab = null;
			var bestShapeIndex: int = -1;
			for(var i: int = 0; i < prefabs.length; ++i)
			{
				var prefab: ShapePrefab = prefabs[i];
				var size: Number = prefab.width * prefab.height;
				if(size > biggestSize)
				{
					biggestSize = size;
					biggestPrefab = prefab;
					bestShapeIndex = i;
				}
			}
			if(bestShapeIndex >= 0)
			{
				prefabs.splice(bestShapeIndex, 1);
			}
			return biggestPrefab;
		}
		
		// convenience stuff to call a function on next redraw
		var afterOneFrameFunction : Function = null;
		private function AfterOneFrame( funcToCall:Function ) : void{
			this.afterOneFrameFunction = funcToCall;
			this.addEventListener(Event.ENTER_FRAME,EnterFrameEvent);
		}
		private function EnterFrameEvent( e:Event) : void{
			this.removeEventListener(Event.ENTER_FRAME,EnterFrameEvent);
			if( this.afterOneFrameFunction != null ){
				var oldFunc : Function = this.afterOneFrameFunction;
				this.afterOneFrameFunction = null;
				oldFunc.call();
				
			}
		}
	}
}