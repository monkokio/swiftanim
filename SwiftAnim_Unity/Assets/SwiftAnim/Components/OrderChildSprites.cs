﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
 #endif

[ExecuteInEditMode]
public class OrderChildSprites : MonoBehaviour {

	public int spriteSortingOrder = 0;
	
	void LateUpdate () {
		DoSorting();
	}
	public void DoSorting () {
		// if parent is sorting, don't sort
		if (this.transform.parent != null && this.transform.parent.GetComponent<OrderChildSprites>() != null) {
			return;
		}

		// order children
		SetChildSortingOrder( this.transform, this.spriteSortingOrder );
	}
	static int SetChildSortingOrder( Transform obj, int depth = 0 ) {

		// skip inactive objects
		if (!obj.gameObject.activeSelf) {
			return depth;
		}

		// set the sprite sortingOrder for this object
		if (obj.GetComponent<SpriteRenderer> () != null) {
			obj.GetComponent<SpriteRenderer> ().sortingOrder = depth;
		}

		// find children in order of Swift Anim z index (Z-depth from Flash)
		int childIndex = 0;
		while (true) {
			Transform child = GetChildAtZIndex ( obj, childIndex++ );
			if (child == null) {
				break;
			}
			depth = SetChildSortingOrder( child, depth + 1 );
		}
		return depth;
	}
	static Transform GetChildAtZIndex ( Transform parentObject, int desiredZIndex ) {
		foreach ( Transform child in parentObject ) {
			if (child.gameObject.activeSelf &&
				child.GetComponent<OrderChildSprites> () != null &&
	  			child.GetComponent<OrderChildSprites> ().spriteSortingOrder == desiredZIndex) 
	  		{
	  			return child;
			}
		}
		return null;
	}
}
