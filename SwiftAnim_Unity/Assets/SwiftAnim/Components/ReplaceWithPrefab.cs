﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
 using UnityEditor;
 using UnityEditor.Callbacks;
 #endif

[ExecuteInEditMode]
public class ReplaceWithPrefab : MonoBehaviour {

	#if UNITY_EDITOR
	public Object prefab = null;

	// Use this for initialization
	public void Start () {
		Replace();
	}
	public void Replace () {
		if (EditorApplication.isPlaying) {
			return;
		}
		if (prefab == null) {
			return;
		}
		GameObject newPrefabInstance = PrefabUtility.InstantiatePrefab ( prefab ) as GameObject;
		if (newPrefabInstance == null) {
			return;
		}
		newPrefabInstance.name = name;
		newPrefabInstance.transform.parent = transform.parent;
		newPrefabInstance.transform.localPosition = transform.localPosition;
		newPrefabInstance.transform.localRotation = transform.localRotation;
		newPrefabInstance.transform.localScale = transform.localScale;
		newPrefabInstance.transform.SetSiblingIndex ( transform.GetSiblingIndex () );
		newPrefabInstance.GetComponent<OrderChildSprites> ().spriteSortingOrder = this.GetComponent<OrderChildSprites> ().spriteSortingOrder;
		newPrefabInstance.SetActive( this.gameObject.activeSelf );

		for ( int i = 0; i < transform.childCount; ++i ) {
			ReplaceWithPrefab childPrefab = transform.GetChild ( i ).GetComponent<ReplaceWithPrefab> ();
			if (childPrefab != null) {
				childPrefab.Replace ();
			}
		}

		GameObject.DestroyImmediate( gameObject );
	}
	#endif
}
