﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(OrderChildSprites))]
public class OrderChildSpritesEditor : Editor {

	public override void OnInspectorGUI () {

		OrderChildSprites targetScript = (OrderChildSprites) target;

		// skip if this is a child
		if (targetScript.transform.parent != null && targetScript.transform.parent.GetComponent<OrderChildSprites> () != null) {

			EditorGUILayout.LabelField ( "Sprite Sorting Order can be set on parent object." );
			return;
		}

		DrawDefaultInspector();

    }
}
