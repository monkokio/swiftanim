using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

[InitializeOnLoad]
public class PrefabGenerator
{
	public const float PIXELS_PER_UNIT = 100.0f;

	public const string COMPONENTS_FOLDER_NAME = "_components";

	// ////////////////////////////////////////////
	// Public API
	public static bool IsAssetValidSwf ( string assetPath ) {
		TextAsset xml;
		return PrefabGenerator.GetSwfAssets( out xml, assetPath );
	}
	public static void QueueSwfForProcessing( string assetPath ){
		swfQueue.Add( assetPath ); 
		if ( EditorApplication.delayCall == null)
        {
			EditorApplication.delayCall -= PrefabGenerator.ProcessAllSwfs;
        }
		EditorApplication.delayCall += PrefabGenerator.ProcessAllSwfs;
	}

	// ////////////////////////////////////////////
	// Public Interface Functions

	private class ChildCurveSet {
		public string childName = "";
		public AnimationCurve enabledCurve = new AnimationCurve();
		public AnimationCurve posXCurve = new AnimationCurve();
		public AnimationCurve posYCurve = new AnimationCurve();
		public AnimationCurve scaleXCurve = new AnimationCurve();
		public AnimationCurve scaleYCurve = new AnimationCurve();
		public AnimationCurve rotXCurve = new AnimationCurve();
		public AnimationCurve rotYCurve = new AnimationCurve();
		public AnimationCurve rotZCurve = new AnimationCurve();
		public AnimationCurve rotWCurve = new AnimationCurve();
		public ChildCurveSet( string n = ""){
			childName = n;
		}
	};
	enum TangentMode
	{
		Editable,
		Smooth,
		Linear,
		Stepped
	}

	static List<string> swfQueue = new List<string>();

	static PrefabGenerator ()
    {
        // initialize static processing queues
    }

    static void ProcessAllSwfs () {
		foreach ( string str in swfQueue ) {
			TextAsset xml;
			UpdateProgressBar(  0.0f,  "generating Unity prefabs" );
			if( GetSwfAssets ( out xml, str ) ){
				ImportSwf( xml );
			}
		}

		EditorUtility.ClearProgressBar();
		swfQueue.Clear();
    }

    private static string progressBarMessage = "";
    private static void UpdateProgressBar( float percent, string message = null ){
    	if( message != null ){
    		PrefabGenerator.progressBarMessage = message;
    	}
		EditorUtility.DisplayProgressBar( "Swift Anim: XML to Unity", progressBarMessage, percent );
    }

	// helper func
	private static bool GetSwfAssets ( out TextAsset xml, string assetPath ) {
		string xmlPath = "";
		if (assetPath.Contains ( ".xml" )) {
			xmlPath = assetPath;
		} else if (assetPath.Contains ( ".swf" )) {
			xmlPath = assetPath.Replace( ".swf", ".xml" );;
		}

		xml = AssetDatabase.LoadAssetAtPath<TextAsset>( xmlPath );
		return xml != null;
	}

	private static void ImportSwf ( TextAsset xmlAsset ) {
		Debug.Assert ( xmlAsset, "Swift Anim: XML invalid" );

		UpdateProgressBar ( 0.05f, xmlAsset.name + ": checking for swf data" );

		// Deserialize XML text into C# data
//		Debug.Log( "Swift Anim: Deserializing XML" );
		SwfData swf = new SwfData ();
		try {
			XmlSerializer serializer = new XmlSerializer ( typeof( SwfData ) );
			using ( StringReader reader = new StringReader ( xmlAsset.text ) ) {
				swf = serializer.Deserialize ( reader ) as SwfData;
			}
		} catch ( System.InvalidOperationException ) {
			return;
		}

		// "clean" all prefab names
		// unity doesn't like some characters in unity assets
		foreach ( MovieClipPrefab mcPrefab in swf.movieClipPrefabs ) {
			mcPrefab.name = CleanPrefabName ( mcPrefab.name );
			foreach ( MovieClipPrefab.Frame frame in mcPrefab.frames ) {
				foreach ( MovieClipPrefab.ChildInstance childInstance in frame.childInstances ) {
					childInstance.prefab = CleanPrefabName ( childInstance.prefab );
				}
			}
		}
		foreach ( ShapePrefab shapePrefab in swf.shapePrefabs ) {
			shapePrefab.name = CleanPrefabName ( shapePrefab.name );
		}

		// create folder directories
		string swfPath = AssetDatabase.GetAssetPath ( xmlAsset ).Replace ( "/" + xmlAsset.name + ".xml", "" );
		string prefabPath = swfPath + "/" + xmlAsset.name;
		string prefabComponentsPath = prefabPath + "/" + COMPONENTS_FOLDER_NAME;
		if (true) {
			if (!AssetDatabase.IsValidFolder ( prefabPath )) {
				AssetDatabase.CreateFolder ( swfPath, xmlAsset.name );
			}

			AssetDatabase.Refresh ();
			prefabComponentsPath = prefabPath + "/" + COMPONENTS_FOLDER_NAME;
			if (!AssetDatabase.IsValidFolder ( prefabComponentsPath )) {
				AssetDatabase.CreateFolder ( prefabPath, COMPONENTS_FOLDER_NAME );
			}
		}

		// create the sprite sheet
		UpdateProgressBar ( 0.1f, xmlAsset.name + ": Sprites" );
		if (true) {
			List<TextureImporter> encounteredPngImporters = new List<TextureImporter> ();

			// generate sprite sheet settings
			foreach ( ShapePrefab shape in swf.shapePrefabs ) {

				Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D> ( swfPath + "/" + shape.textureName );
				Debug.Assert ( texture, "Could not get texture [" + shape.textureName + "] for sprite" );
				TextureImporter textureImporter = AssetImporter.GetAtPath ( swfPath + "/" + shape.textureName ) as TextureImporter;
				Debug.Assert ( textureImporter, "Could not get importer for [" + shape.textureName + "]" );
				List<SpriteMetaData> sprites = new List<SpriteMetaData> ();
				if (!encounteredPngImporters.Contains ( textureImporter )) {
					// first time encountering this texture, initialize spritesheet
					textureImporter.spriteImportMode = SpriteImportMode.Multiple;
					textureImporter.alphaIsTransparency = true;
					encounteredPngImporters.Add ( textureImporter );
				} else {
					// we've seen this texture before, load sprites
					foreach ( SpriteMetaData sprite in textureImporter.spritesheet ) {
						sprites.Add ( sprite );
					}
				}

				SpriteMetaData newSprite = new SpriteMetaData ();
				newSprite.name = shape.name;
				newSprite.rect = new Rect ( shape.textureX, texture.height - shape.textureY - shape.textureHeight, shape.textureWidth, shape.textureHeight );
				newSprite.alignment = (int) SpriteAlignment.Custom;
				newSprite.pivot = new Vector2 ( shape.originX / shape.textureWidth, 1.0f - shape.originY / shape.textureHeight );

				sprites.Add ( newSprite );
				textureImporter.spritesheet = sprites.ToArray ();
			}

			// re-import sprite sheet using new settings
			foreach ( TextureImporter pngImporter in encounteredPngImporters ) {
				pngImporter.SaveAndReimport ();
			}

		}

		UpdateProgressBar ( 0.2f, xmlAsset.name + ": Prefabs" );

		// make the MC prefabs
		foreach ( MovieClipPrefab mc in swf.movieClipPrefabs ) {

			// create new Game Object for the prefab
			GameObject newMcPrefab = new GameObject ( mc.name );
			newMcPrefab.AddComponent<OrderChildSprites> ();

			// Create or replace the prefab
			mc.unityPrefab = GetMcPrefabAsset ( prefabPath, prefabComponentsPath, mc.name );
			if (mc.unityPrefab == null) {
				mc.unityPrefab = PrefabUtility.CreatePrefab ( GetMcPrefabAssetPath ( prefabPath, prefabComponentsPath, mc.name ), newMcPrefab );
			} else {
				mc.unityPrefab = PrefabUtility.ReplacePrefab ( newMcPrefab, mc.unityPrefab, ReplacePrefabOptions.ReplaceNameBased );
			}

			GameObject.DestroyImmediate ( newMcPrefab );
		}

		UpdateProgressBar ( 0.3f, xmlAsset.name + ": Adding Children" );
		// add all children instances to all MC prefabs
		AssetDatabase.Refresh ();
		foreach ( MovieClipPrefab mc in swf.movieClipPrefabs ) {

			// get the prefab
			Debug.Assert ( mc.unityPrefab, "could not load prefab: " + mc.name );
			GameObject obj = PrefabUtility.InstantiatePrefab ( mc.unityPrefab ) as GameObject;
			Debug.Assert ( obj, "prefab instance not a GameObject: " + mc.name + ".prefab" );


			// go through every frame to spawn all children instances
			foreach ( MovieClipPrefab.Frame frame in mc.frames ) {
				foreach ( MovieClipPrefab.ChildInstance childInstance in frame.childInstances ) {
					
					// create the child obj, if it wasn't already created on a prev frame
					if (obj.transform.Find ( childInstance.name ) == null) {

						// create a child object
						GameObject childObject = null;

						// Spawn sprite child
						if (childInstance.type == MovieClipPrefab.ChildInstanceType.shape) {
							ShapePrefab shapePrefab = swf.FindPrefabByName ( childInstance.prefab );
							childObject = new GameObject ( childInstance.name );
							childObject.AddComponent<OrderChildSprites> ();
							SpriteRenderer spriteRenderer = childObject.AddComponent<SpriteRenderer> ();
							spriteRenderer.sprite = LoadSpriteAsset ( swfPath + "/" + shapePrefab.textureName, childInstance.prefab );
							Debug.Assert ( spriteRenderer.sprite, "Swift Anim: Could not find sprite " + childInstance.prefab );

						} 
						// spawn MC child
						else if (childInstance.type == MovieClipPrefab.ChildInstanceType.movieClip) {

							Object childPrefab = GetMcPrefabAsset ( prefabPath, prefabComponentsPath, childInstance.prefab );
							childObject = PrefabUtility.InstantiatePrefab ( childPrefab ) as GameObject;
							childObject.AddComponent<ReplaceWithPrefab> ().prefab = childPrefab; // hack - nested prefabs not supported by unity
						} else {
							Debug.Assert ( false, mc.name + "/" + childInstance.name + " does not have a known type" );
						}

						childObject.name = childInstance.name;
						childObject.transform.SetParent ( obj.transform );
						childObject.transform.localPosition = new Vector2 ( childInstance.x / PIXELS_PER_UNIT, -childInstance.y / PIXELS_PER_UNIT );
						childObject.transform.localScale = new Vector2 ( childInstance.scaleX, childInstance.scaleY );
						childObject.transform.localRotation = Quaternion.Euler ( 0, 0, -childInstance.rotation );
						childObject.SetActive ( frame.frameNumber == 0 );
						childObject.GetComponent<OrderChildSprites> ().spriteSortingOrder = childInstance.zIndex;

					}
				}
			}

			// save the prefab
			PrefabUtility.ReplacePrefab ( obj, mc.unityPrefab, ReplacePrefabOptions.ReplaceNameBased );
			GameObject.DestroyImmediate ( obj );
		}

		UpdateProgressBar ( 0.5f, xmlAsset.name + ": Depth Sorting" );
		// go through MC prefabs and sort children sprites
		foreach ( MovieClipPrefab mc in swf.movieClipPrefabs ) {
			
			Debug.Assert ( mc.unityPrefab, "could not load prefab:" + mc.name );
			GameObject obj = PrefabUtility.InstantiatePrefab ( mc.unityPrefab ) as GameObject;

			// sort children for prefab
			SpriteRenderer[] allChildSprites = obj.GetComponentsInChildren<SpriteRenderer> ();
			int depth = 0;
			foreach ( SpriteRenderer sprite in allChildSprites ) {
				sprite.sortingOrder = depth++;
			}

			// save prefab
			PrefabUtility.ReplacePrefab ( obj, mc.unityPrefab, ReplacePrefabOptions.ReplaceNameBased );
			GameObject.DestroyImmediate ( obj );
		}

		UpdateProgressBar ( 0.7f, xmlAsset.name + ": Animations" );
		// go through MC prefabs and make animations
		foreach ( MovieClipPrefab mc in swf.movieClipPrefabs ) {

			// don't create animation clips for movieclips that are only 1 frame
			if (mc.frames.Count <= 1) {
				continue;
			}

			// find game object
			Debug.Assert ( mc.unityPrefab, "could not load prefab:" + mc.name );
			GameObject movieClipObj = PrefabUtility.InstantiatePrefab ( mc.unityPrefab ) as GameObject;

			// add animator
			string animControllerPath = prefabComponentsPath + "/" + mc.name + "_AnimController.controller";
			AnimatorController animController = AssetDatabase.LoadAssetAtPath<AnimatorController> ( animControllerPath );
			if (animController == null) {
				animController = AnimatorController.CreateAnimatorControllerAtPath ( animControllerPath );
			}
			Animator animator = movieClipObj.GetComponent<Animator> ();
			if (animator == null) {
				animator = movieClipObj.AddComponent<Animator> ();
			}
			animator.runtimeAnimatorController = animController;


			// create a set of curves for each child object
			List< ChildCurveSet > childCurves = new List< ChildCurveSet > ();
			for ( int i = 0; i < movieClipObj.transform.childCount; i++ ) {
				GameObject childObject = movieClipObj.transform.GetChild ( i ).gameObject;
				childCurves.Add ( new ChildCurveSet ( childObject.name ) );
			}

			// set "current animation clip" to the first animation clip
			string currentFrameLabel = mc.frames [ 0 ].label;
			AnimatorState previousAnimState = null;
			AnimatorState firstAnimState = null;
			int animClipStartFrame = 0;

			// go through every frame and add keyframes to the curves
			foreach ( MovieClipPrefab.Frame frame in mc.frames ) {

				// if we are starting off a new animation clip, save off the previous one
				if (frame.label != currentFrameLabel) {
					previousAnimState = CreateAnimationClipFromCurves ( currentFrameLabel, childCurves, mc, prefabComponentsPath, animController, previousAnimState, swf.fps );
					if (firstAnimState == null) {
						firstAnimState = previousAnimState;
					}
					currentFrameLabel = frame.label;
					animClipStartFrame = frame.frameNumber;
				}

				float frameTime = ( frame.frameNumber - animClipStartFrame ) / swf.fps;

				// reset list of all current children sprites & movieclips
				List<GameObject> unAnimatedChildren = new List< GameObject> ();
				for ( int i = 0; i < movieClipObj.transform.childCount; i++ ) {
					unAnimatedChildren.Add ( movieClipObj.transform.GetChild ( i ).gameObject );
				}

				// go through each child instance that is animated this frame
				foreach ( MovieClipPrefab.ChildInstance childInstanceInfo in frame.childInstances ) {

					// find the child, mark as "animated this frame"
					Debug.Assert ( movieClipObj.transform.Find ( childInstanceInfo.name ), movieClipObj.name + "/" + childInstanceInfo.name + " (frame " + frame.frameNumber + ") not found!" );
					GameObject childObject = movieClipObj.transform.Find ( childInstanceInfo.name ).gameObject;
					unAnimatedChildren.Remove ( childObject );
					ChildCurveSet curve = FindChildCurvesWithName ( childCurves, childObject.name );

					curve.enabledCurve.AddKey ( new Keyframe ( frameTime, 1, float.PositiveInfinity, float.PositiveInfinity ) );
					curve.posXCurve.AddKey ( new Keyframe ( frameTime, childInstanceInfo.x / PIXELS_PER_UNIT, 0.0f, 0.0f ) );
					curve.posYCurve.AddKey ( new Keyframe ( frameTime, -childInstanceInfo.y / PIXELS_PER_UNIT, 0.0f, 0.0f ) );
					curve.scaleXCurve.AddKey ( new Keyframe ( frameTime, childInstanceInfo.scaleX, 0.0f, 0.0f ) );
					curve.scaleYCurve.AddKey ( new Keyframe ( frameTime, childInstanceInfo.scaleY, 0.0f, 0.0f ) );
					Quaternion rot = Quaternion.Euler ( 0, 0, -childInstanceInfo.rotation );
					curve.rotXCurve.AddKey ( new Keyframe ( frameTime, rot.x, 0.0f, 0.0f ) );
					curve.rotYCurve.AddKey ( new Keyframe ( frameTime, rot.y, 0.0f, 0.0f ) );
					curve.rotZCurve.AddKey ( new Keyframe ( frameTime, rot.z, 0.0f, 0.0f ) );
					curve.rotWCurve.AddKey ( new Keyframe ( frameTime, rot.w, 0.0f, 0.0f ) );

//					// if child was hidden last frame, copy curve into previous keyframe
					int currentKeyIdx = curve.enabledCurve.length - 1;
					int prevKeyIdx = currentKeyIdx - 1;
					if (prevKeyIdx >= 0 && curve.enabledCurve.keys [ prevKeyIdx ].value == 0) {
						CopyAnimationKeyframe ( ref curve, currentKeyIdx, prevKeyIdx, true );
					}
				}

				// remaining un-animated children should be disabled this frame
				foreach ( GameObject childObject in unAnimatedChildren ) {
					ChildCurveSet curve = FindChildCurvesWithName ( childCurves, childObject.name );
					AddEmptyAnimationKeyframe ( ref curve, swf.fps, frameTime, true );
				}
			}
			AnimatorState finalAnimState = CreateAnimationClipFromCurves ( currentFrameLabel, childCurves, mc, prefabComponentsPath, animController, previousAnimState, swf.fps );
			CreateAnimationTransition ( finalAnimState, firstAnimState );

			// save prefab
			PrefabUtility.ReplacePrefab ( movieClipObj, mc.unityPrefab, ReplacePrefabOptions.ReplaceNameBased );
			GameObject.DestroyImmediate ( movieClipObj );
		}


		UpdateProgressBar ( 0.8f, xmlAsset.name + ": Refresh prefab children" );
		// this is a bit of a hack because nested prefabs doesn't  work in Unity.
		// now that prefabs are generated fully, make sure each prefab child is expanded/refreshed
		foreach ( MovieClipPrefab mc in swf.movieClipPrefabs ) {
			
			Debug.Assert ( mc.unityPrefab, "could not load prefab:" + mc.name );
			GameObject obj = PrefabUtility.InstantiatePrefab ( mc.unityPrefab ) as GameObject;

			// replace children with up-to-date prefabs
			for ( int i = 0; i < obj.transform.childCount; ++i ) {
				ReplaceWithPrefab childPrefab = obj.transform.GetChild ( i ).GetComponent<ReplaceWithPrefab> ();
				if (childPrefab != null) {
					childPrefab.Replace ();
				}
			}

			// save prefab
			PrefabUtility.ReplacePrefab ( obj, mc.unityPrefab, ReplacePrefabOptions.ReplaceNameBased );
			GameObject.DestroyImmediate ( obj );
		}


		UpdateProgressBar( 0.9f, xmlAsset.name + ": saving" );
		Debug.Log( "Swift Anim by Bryan Singh  :  Imported \"" + xmlAsset.name + ".swf\"" );
		AssetDatabase.DeleteAsset( AssetDatabase.GetAssetPath( xmlAsset ) );
		AssetDatabase.SaveAssets();

	}


	private static string CleanPrefabName ( string prefabName ) {
		return prefabName.Replace(":","_");
	}
	private static string GetMcPrefabAssetPath ( string prefabPath, string prefabComponentsPath, string  mcName ) {
		if (mcName.Contains ( "instance" )) {
			return prefabComponentsPath + "/" + mcName + ".prefab";
		} else {

			return prefabPath + "/" + mcName + ".prefab";
		}
	}
	private static Object GetMcPrefabAsset ( string prefabPath, string prefabComponentsPath, string  mcName ) {
		return AssetDatabase.LoadAssetAtPath<Object>( GetMcPrefabAssetPath( prefabPath, prefabComponentsPath, mcName ));
	}
	private static Sprite LoadSpriteAsset ( string texturePath, string spriteName ) {
		Debug.Assert( AssetDatabase.LoadAssetAtPath<Texture2D>(texturePath), "Texture does not exist! [" + texturePath + "]" );
		Object[] assets = AssetDatabase.LoadAllAssetsAtPath ( texturePath );
		Debug.Assert( assets != null, "Swift Anim: Could not get texture from asset database [" + texturePath + "]" );
		foreach ( Object asset in assets ) {
			if( asset is Sprite ){
				Sprite sprite =  asset as Sprite;
				if (sprite.name == spriteName) {
					return sprite;
				}
			}
		}
		Debug.Assert( false, "Swift Anim: Sprite \"" + spriteName + "\" not found" );
		return null;
	}

	private static ChildCurveSet FindChildCurvesWithName( List<ChildCurveSet> childCurves, string objName ){
		foreach ( ChildCurveSet c in childCurves ) {
			if (c.childName == objName) {
				return c;
			}
		}
		return null;
	}

	private static void AddEmptyAnimationKeyframe ( ref ChildCurveSet curveSet, float fps, float frameTime = -1.0f, bool forceDisabled = false ) {

		// compute keyframe time
		if (frameTime < 0.0f) {
			if (curveSet.enabledCurve.keys.Length > 0) {
				frameTime = curveSet.enabledCurve.keys [ curveSet.enabledCurve.keys.Length - 1 ].time + 1.0f / fps;
			} else {
				frameTime = 0.0f;
			}
		}

		// add new keyframes
		curveSet.enabledCurve.AddKey ( new Keyframe ( frameTime, 0, float.PositiveInfinity, float.PositiveInfinity ) );
		curveSet.posXCurve.AddKey ( new Keyframe ( frameTime, 0.0f, 0.0f, 0.0f ) );
		curveSet.posYCurve.AddKey ( new Keyframe ( frameTime, 0.0f, 0.0f, 0.0f ) );
		curveSet.scaleXCurve.AddKey ( new Keyframe ( frameTime, 1.0f, 0.0f, 0.0f ) );
		curveSet.scaleYCurve.AddKey ( new Keyframe ( frameTime, 1.0f, 0.0f, 0.0f ) );
		curveSet.rotXCurve.AddKey ( new Keyframe ( frameTime, 0.0f, 0.0f, 0.0f ) );
		curveSet.rotYCurve.AddKey ( new Keyframe ( frameTime, 0.0f, 0.0f, 0.0f ) );
		curveSet.rotZCurve.AddKey ( new Keyframe ( frameTime, 0.0f, 0.0f, 0.0f ) );
		curveSet.rotWCurve.AddKey ( new Keyframe ( frameTime, 1.0f, 0.0f, 0.0f ) );

		// if there is a previous frame, copy its values
		if (curveSet.enabledCurve.keys.Length >= 2) {
			
			int currentIndex = curveSet.enabledCurve.keys.Length - 1;
			int previousIndex = curveSet.enabledCurve.keys.Length - 2;
			CopyAnimationKeyframe ( ref curveSet, previousIndex, currentIndex, forceDisabled );
		}
	}
	private static void CopyAnimationKeyframe ( ref ChildCurveSet curveSet, int sourceIndex, int destinationIndex, bool forceDisabled = false ) {
		
		SetCurveKeyValue( ref curveSet.enabledCurve, destinationIndex, forceDisabled ? 0 : curveSet.enabledCurve.keys [ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.posXCurve   , destinationIndex, curveSet.posXCurve   .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.posYCurve   , destinationIndex, curveSet.posYCurve   .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.scaleXCurve , destinationIndex, curveSet.scaleXCurve .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.scaleYCurve , destinationIndex, curveSet.scaleYCurve .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.rotXCurve   , destinationIndex, curveSet.rotXCurve   .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.rotYCurve   , destinationIndex, curveSet.rotYCurve   .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.rotZCurve   , destinationIndex, curveSet.rotZCurve   .keys[ sourceIndex ].value);
		SetCurveKeyValue( ref curveSet.rotWCurve   , destinationIndex, curveSet.rotWCurve   .keys[ sourceIndex ].value);

	}
	private static void SetCurveKeyValue ( ref AnimationCurve curve, int keyIndex, float value ) {
		Keyframe key = curve.keys[keyIndex];
		key.value = value;
		curve.RemoveKey( keyIndex );
		curve.AddKey( key );
	}

	private static AnimationCurve OptimizeCurve ( ref AnimationCurve curve ) {
		int index = 2;
		while (index < curve.keys.Length) {
			float firstValue = curve.keys [ index - 2 ].value;
			float middleValue = curve.keys [ index - 1 ].value;
			float lastValue = curve.keys [ index ].value;
			if (Mathf.Approximately ( middleValue, firstValue ) && Mathf.Approximately ( middleValue, lastValue )) {
				curve.RemoveKey( index-1 );
				continue;
			} else {
				index++;
				continue;
			}
		}
		return curve;
	}

	private static AnimatorState CreateAnimationClipFromCurves ( string clipName, List< ChildCurveSet > childCurves, MovieClipPrefab mc, string prefabPath, AnimatorController animController, AnimatorState prevState, float fps ) {

		string animClipPath = prefabPath + "/" + mc.name + "_" + clipName + ".anim";
		AnimationClip animClip = AssetDatabase.LoadAssetAtPath<AnimationClip> ( animClipPath );
		if (animClip == null) {
			animClip = new AnimationClip ();
			AssetDatabase.CreateAsset ( animClip, animClipPath );
		}

		// add sprite curves to this animation clip
		for( int i = 0;i < childCurves.Count; ++i ){
			ChildCurveSet curve = childCurves[i];
			Debug.Assert ( curve.enabledCurve.keys.Length > 0, "Animation for " + mc.name + "." + curve.childName + " does not have keyframes!" );

			// duplicate last keyframe
			AddEmptyAnimationKeyframe( ref curve, fps );

			// add the curves to the animation clip
			curve.enabledCurve.postWrapMode = WrapMode.Loop;
			animClip.SetCurve ( curve.childName, typeof( GameObject ), "m_IsActive", OptimizeCurve( ref curve.enabledCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalPosition.x", OptimizeCurve( ref curve.posXCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalPosition.y", OptimizeCurve( ref curve.posYCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalScale.x", OptimizeCurve( ref curve.scaleXCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalScale.y", OptimizeCurve( ref curve.scaleYCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalRotation.x", OptimizeCurve( ref curve.rotXCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalRotation.y", OptimizeCurve( ref curve.rotYCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalRotation.z", OptimizeCurve( ref curve.rotZCurve ));
			animClip.SetCurve ( curve.childName, typeof( Transform ), "m_LocalRotation.w", OptimizeCurve( ref curve.rotWCurve ));

		}
		animClip.wrapMode = WrapMode.Loop;
		AnimationClipSettings animClipSettings = AnimationUtility.GetAnimationClipSettings ( animClip );
		animClipSettings.loopTime = true;

		// find/create the state machine state
		AnimationUtility.SetAnimationClipSettings ( animClip, animClipSettings );
		AnimatorState animState = FindControllerState ( clipName, animController );
		if (animState == null) {
			animState = animController.AddMotion ( animClip );
			animState.name = clipName;
		}

		CreateAnimationTransition( prevState, animState );

		// reset the curves (this function consumes them)
		for ( int i = 0; i < childCurves.Count; ++i ) {
			childCurves[i] = new ChildCurveSet( childCurves[i].childName );
		}

		return animState;
	}
	static private AnimatorStateTransition CreateAnimationTransition ( AnimatorState fromState, AnimatorState toState ) {
		if (fromState != null && toState != null ) {
			AnimatorStateTransition transition = FindAnimStateTransition( fromState, toState );
			if (transition == null) {
				transition = fromState.AddTransition( toState );
				transition.duration = 0.0f;
				transition.hasFixedDuration = true;
				transition.hasExitTime = true;
				transition.exitTime = 1.0f;
			}
			return transition;
		}
		return null;
	}
	static private  AnimatorState FindControllerState( string stateName, AnimatorController controller ){
		foreach ( ChildAnimatorState state in controller.layers [ 0 ].stateMachine.states ) {
			if (state.state.name == stateName) {
				return state.state;
			}
		}
		return null;
	}

	static private  AnimatorStateTransition FindAnimStateTransition( AnimatorState fromState,  AnimatorState toState ){
		foreach ( AnimatorStateTransition transition in fromState.transitions ) {
			
			if (transition.destinationState == toState) {
				return transition;
			}
		}
		return null;
	}
}