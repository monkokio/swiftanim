﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

[XmlType("shape")]
public class ShapePrefab {
	[XmlAttribute] public string name = "";
	[XmlAttribute] public int textureX = 0;
	[XmlAttribute] public int textureY = 0;
	[XmlAttribute] public string textureName = "";
	[XmlAttribute] public int textureWidth = 0;
	[XmlAttribute] public int textureHeight = 0;
	[XmlAttribute] public float originX = 0.0f;
	[XmlAttribute] public float originY = 0.0f;
}
