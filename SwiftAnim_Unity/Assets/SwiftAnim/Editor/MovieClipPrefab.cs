﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[XmlType("movieClip")]
public class MovieClipPrefab {

	public enum ChildInstanceType{
		movieClip,
		shape,
		undefined
	}

	[XmlType("child")]
	public class ChildInstance{
		[XmlAttribute] public int zIndex = 0;
		[XmlAttribute] public string name = "";
		[XmlAttribute] public string prefab = "";
		[XmlAttribute] public float x = 0;
		[XmlAttribute] public float y = 0;
		[XmlAttribute] public float scaleX = 0;
		[XmlAttribute] public float scaleY = 0;
		[XmlAttribute] public float rotation = 0;
		[XmlAttribute] public ChildInstanceType type = ChildInstanceType.undefined;
	}


	[XmlType("frame")]
	public class Frame{
		[XmlAttribute] public int frameNumber = 0;
		[XmlAttribute] public string label = "";
		public List<ChildInstance> childInstances = new List<ChildInstance>();

	}
	[XmlAttribute] public string name = "";
	public List<Frame> frames = new List<Frame>();

	public Object unityPrefab = null;

}
