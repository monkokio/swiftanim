using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

// @TODO Check for MCs w/ dif prefabs and same names (this tool will not support that)
// @TODO Support re-importing swf files

public class SwfPostProcessor : AssetPostprocessor 
{

	public const string SWF_TO_XML_DIR = "SwiftAnim/SwfToXml";
	public const string SWF_TO_XML_PATH_MAC_ZIP = "SwiftAnim/SwfToXml/swfToXml_Mac.zip";
    public const string SWF_TO_XML_PATH_WIN_ZIP = "SwiftAnim/SwfToXml/swfToXml_Win.zip";
    public const string SWF_TO_XML_FOLDER_MAC = "SwiftAnim/SwfToXml/Mac~";
    public const string SWF_TO_XML_FOLDER_WIN = "SwiftAnim/SwfToXml/Win~";
    public const string SWF_TO_XML_PATH_MAC = "SwiftAnim/SwfToXml/Mac~/swfToXml_Mac.app";
	public const string SWF_TO_XML_PATH_WIN = "SwiftAnim/SwfToXml/Win~/swfToXml.exe";
    public const string ZIP_EXE_PATH_WIN = "SwiftAnim/SwfToXml/7za.exe";

    // Automatically post-process XML files that are detected
    static void OnPostprocessAllAssets ( string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths ) {
		List<string> swfFiles = new List<string> ();
		// if a new XML file is imported, queue for processing
		foreach ( string str in importedAssets ) {
			if (str.Contains ( ".xml" ) && PrefabGenerator.IsAssetValidSwf ( str )) {
				PrefabGenerator.QueueSwfForProcessing ( str );
			} else if (str.Contains ( ".swf" ) && !str.Contains ( ".app" )) {
				if (str.ToLower ().Replace ( " ", "" ).Contains ( "noimport" )) {
					Debug.Log ( "Swift Anim: file ignored because it was flagged as 'No Import' (" + str + ")" );
					continue;
				}
				string swfFilePath = Application.dataPath.Replace ( "/Assets", "/" + str );
				swfFiles.Add ( swfFilePath );
			}
		}

		if (swfFiles.Count > 0) {
			

			string swfToXmlPath = "";
			string argsStr = "";
			if (Application.platform == RuntimePlatform.WindowsEditor) {
				swfToXmlPath = Application.dataPath + "/" + SWF_TO_XML_PATH_WIN;

				foreach ( string swfFile in swfFiles ) {
					argsStr += " \"" + swfFile + "\"";
				}
				swfToXmlPath = swfToXmlPath.Replace ( "/", "\\" );
				argsStr = argsStr.Replace ( "/", "\\" );

                // unzip the SwfToXml tool
                string swfToXmlFolder = (Application.dataPath + "/" + SWF_TO_XML_FOLDER_WIN).Replace("/", "\\");
                if (!Directory.Exists(swfToXmlFolder))
                {
                    EditorUtility.DisplayProgressBar("Swift Anim", "unzipping swf-to-xml app", 0.5f);
                    string zipUtil = ( '"' + Application.dataPath + "/" + ZIP_EXE_PATH_WIN + '"' ).Replace("/", "\\");
                    string zipPath = ( '"' + Application.dataPath + "/" + SWF_TO_XML_PATH_WIN_ZIP + '"').Replace("/", "\\");
                    string unzipPath = ( '"' + Application.dataPath + "/" + SWF_TO_XML_FOLDER_WIN + '"' ).Replace("/", "\\");

                    // 7za x swfToXml_Win.zip - o"C:\Users\Crista\Documents\SwiftAnim\SwiftAnim_Unity\Assets\SwiftAnim\SwfToXml\Win~"

                        
                    System.Diagnostics.Process.Start(zipUtil, " x " + '"' + zipPath + '"' + " -o" + '"' + unzipPath + '"').WaitForExit();
                    EditorUtility.ClearProgressBar();
                }
            } else if (Application.platform == RuntimePlatform.OSXEditor) {
				swfToXmlPath = Application.dataPath + "/" + SWF_TO_XML_PATH_MAC;
				argsStr = "--args";
				foreach ( string swfFile in swfFiles ) {
					argsStr += " \"" + swfFile + "\"";
				}

                // unzip the SwfToXml tool
				if ( ! Directory.Exists ( Application.dataPath + "/" + SWF_TO_XML_PATH_MAC )) {
					EditorUtility.DisplayProgressBar( "Swift Anim", "unzipping swf-to-xml app", 0.5f );
					string zipPath = "\"" + Application.dataPath + "/" + SWF_TO_XML_PATH_MAC_ZIP + "\"";
					string unzipPath = "\"" + Application.dataPath + "/" + SWF_TO_XML_FOLDER_MAC + "\"";
					System.Diagnostics.Process.Start ( "unzip" , zipPath + " -d " + unzipPath ).WaitForExit();
					EditorUtility.ClearProgressBar();
				}

			}

			System.Diagnostics.Process.Start ( swfToXmlPath, argsStr).WaitForExit();
		}
	}

}