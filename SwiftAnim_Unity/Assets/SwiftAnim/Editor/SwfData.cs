﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[XmlType("swfData")]
public class SwfData {
	[XmlAttribute] public float fps = 24.0f;
	public List<ShapePrefab> shapePrefabs = new List<ShapePrefab>();
	public List<MovieClipPrefab> movieClipPrefabs = new List<MovieClipPrefab>();

	public ShapePrefab FindPrefabByName ( string prefabName ) {
		for ( int i = 0; i < shapePrefabs.Count; ++i ) {
			if (shapePrefabs [ i ].name == prefabName) {
				return shapePrefabs [ i ];
			}
		}
		Debug.Assert( false, "Does not contain a Shape prefab called: " + prefabName );
		return null;
	}
}
