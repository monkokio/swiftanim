﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Animator))]

public class GreghoryMovement : MonoBehaviour {
	
	public float movementSpeed = 2.0f;
	private Vector3 velocity = new Vector3(0, 0, 0);
	private Vector3 lastWalkingVelocity = new Vector3(1.0f, 0, 0);
	
	// Update is called once per frame
	void Update () {

		// move with arrow keys
		Vector3 desiredVelocity = new Vector3 ( 0, 0, 0 );
		if (Input.GetKey ( KeyCode.LeftArrow )) {
			desiredVelocity += Vector3.left;
		}
		if (Input.GetKey ( KeyCode.RightArrow )) {
			desiredVelocity += Vector3.right;
		}
		if (Input.GetKey ( KeyCode.UpArrow )) {
			desiredVelocity += Vector3.up;
		}
		if (Input.GetKey ( KeyCode.DownArrow )) {
			desiredVelocity += Vector3.down;
		}
		desiredVelocity = movementSpeed * Vector3.ClampMagnitude ( desiredVelocity, 1.0f );
		this.velocity = Vector3.Lerp ( this.velocity, desiredVelocity, Time.deltaTime * 10.0f );
		this.transform.position = this.transform.position + this.velocity * Time.deltaTime;

		// select animation state -
		//  Transitions were deleted from the Animator Controller so animation states could be chosen here.
		//  Another option would be to set animator variables and have the Animator Controller drive animation states.
		Animator animator = this.GetComponent<Animator> ();
		if (Vector3.Magnitude ( this.velocity ) < 0.5f) {
			if (Mathf.Abs ( this.lastWalkingVelocity.x ) > Mathf.Abs ( this.lastWalkingVelocity.y )) {	
				if (this.lastWalkingVelocity.x < 0.0f) {
					animator.Play ( "standLeft" );
				} else {
					animator.Play ( "standRight" );
				}
			} else {
				if (this.lastWalkingVelocity.y < 0.0f) {
					animator.Play ( "standDown" );
				} else {
					animator.Play ( "standUp" );
				}
			}
		} else {
			this.lastWalkingVelocity = this.velocity;
			if (Mathf.Abs ( this.velocity.x ) > Mathf.Abs ( this.velocity.y )) {	
				if (this.velocity.x < 0.0f) {
					animator.Play ( "walkLeft" );
				} else {
					animator.Play ( "walkRight" );
				}
			} else {
				if (this.velocity.y < 0.0f) {
					animator.Play ( "walkDown" );
				} else {
					animator.Play ( "walkUp" );
				}
			}
		}
	}
}
